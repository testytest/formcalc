## Калькулятор формул  
**При вводе необходимых переменных этот калькулятор высчитывает ответ по заданным формулам**  
Скомпилированный EXE - http://bit.ly/formcalc  
Virustotal - http://bit.ly/formcalc-vt  
*Python 3.6*   
*Kornouhov Roman, 2018*